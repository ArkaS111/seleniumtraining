package training.selenium.practice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TimesOfIndia {
	WebDriver driver;

	TimesOfIndia() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		System.setProperty("webdriver.chrome.driver",
				"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver(options);
	}

	// navigate to indiatoday website
	public void navigation(WebDriver driver) {
		driver.navigate().to("https://www.indiatoday.in/news.html");
	}

	// take out all the elements of the sports section and put them in sports
	// arraylist
	public void printSportsHeadlines(WebDriver driver) {
		List<WebElement> sports = new ArrayList<WebElement>();
		sports = driver.findElements(By.id("card_1206550_itg-news-section-4"));
		String[] sportsFour = (sports.get(sports.size() - 1).getText()).split("\\r?\\n|\\r");
		System.out.println("Sports////////");
		for (int j = 0; j < 4; j++) {
			System.out.println(sportsFour[j]);
		}
	}

	public void printMoviesHeadlines(WebDriver driver) {
		List<WebElement> movies = new ArrayList<WebElement>();
		movies = driver.findElements(By.id("card_1206533_itg-news-section-2"));
		// int m = ;
		String[] moviesFour = (movies.get(movies.size() - 1).getText()).split("\\r?\\n|\\r");
		System.out.println("Movies////////");
		for (int j = 0; j < 4; j++) {
			System.out.println(moviesFour[j]);
		}
	}

	public void closeDriver(WebDriver driver) {
		driver.close();
	}

}
