package training.selenium.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleSearchPage {
	public WebElement returnFirstSearchResult(WebDriver driver) {
		return driver.findElement(By.xpath("//div[@class=\"g\"]/div/div/div[@class=\"yuRUbf\"]"));
	}
	

}
