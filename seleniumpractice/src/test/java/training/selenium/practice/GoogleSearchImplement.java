package training.selenium.practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class GoogleSearchImplement {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		WebDriver driver = new ChromeDriver();
		GoogleHomePage home = new GoogleHomePage();
		home.navigateToGoogle(driver);
		WebElement searchBox = home.returnSearchBox(driver);
		searchBox.sendKeys("Java");
		WebElement searchButton = home.returnSearchButton(driver);
		searchButton.click();
		GoogleSearchPage gsp = new GoogleSearchPage();
		WebElement firstResult = gsp.returnFirstSearchResult(driver);
		System.out.println(firstResult.getText());
	}

}
