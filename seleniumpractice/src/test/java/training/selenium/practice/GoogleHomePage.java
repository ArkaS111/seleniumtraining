package training.selenium.practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class GoogleHomePage {

	public void navigateToGoogle(WebDriver driver) {
		driver.navigate().to("https://www.google.com");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public WebElement returnSearchBox(WebDriver driver) {
		return driver.findElement(By.name("q"));
	}
	public WebElement returnSearchButton(WebDriver driver) {
		return driver.findElement(By.name("btnK"));

	}

}
