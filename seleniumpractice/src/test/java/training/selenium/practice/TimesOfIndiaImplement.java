package training.selenium.practice;

public class TimesOfIndiaImplement {

	public static void main(String[] args) {
		TimesOfIndia toi = new TimesOfIndia();
		toi.navigation(toi.driver);
		toi.printSportsHeadlines(toi.driver);
		toi.printMoviesHeadlines(toi.driver);
		toi.closeDriver(toi.driver);

	}

}
