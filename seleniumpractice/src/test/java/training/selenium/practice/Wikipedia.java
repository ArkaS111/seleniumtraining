package training.selenium.practice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Wikipedia {
	WebDriver driver;
	String link = "";
	List<String> url = new ArrayList<>();

	Wikipedia() {
//		System.setProperty("webdriver.chrome.driver",
//				"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
//		ChromeOptions options = new ChromeOptions();
//		options.addArguments("--headless");
//		driver = new ChromeDriver(options);
		System.setProperty("webdriver.gecko.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\geckodriver-v0.29.1-win64\\geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--headless");
		driver = new FirefoxDriver(options);
		
	}

	public void openWikipedia() {
		driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
	}

	public void pickLinks() throws InterruptedException {
		List<WebElement> links = new ArrayList<WebElement>();

		links = driver.findElements(By.xpath("//*[@id=\"mp-tfa\"]/p/a"));
		for (int i = 1; i <= links.size(); i++) {
			 String link = ((links.get(i - 1).getAttribute("href")));
			url.add(link);
		}
	}

	public void getLinkTitle() {
		for (int i = 0; i < 10; i++) {
			System.out.print("Link: " + url.get(i));
			driver.navigate().to(url.get(i));
			System.out.print("  Link Page Title: ");
			System.out.println(driver.getTitle());

		}

	}

	public static void main(String[] args) throws InterruptedException {
		Wikipedia wk = new Wikipedia();
		wk.openWikipedia();
		wk.pickLinks();
		wk.getLinkTitle();
	}

}
