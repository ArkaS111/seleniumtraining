package training.selenium.practice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

public class WebsiteTestCsv {
	public static void main(String[] args) throws IOException {
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless"); 
        System.setProperty("webdriver.chrome.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);
		FileReader fr = new FileReader("D:\\arka\\Documents\\website.csv");
		BufferedReader br = new BufferedReader(fr);
		String line;
		String[] websites = { "" };
		while ((line = br.readLine()) != null) {
			line = br.readLine();
			websites = line.split(",");
			String url = websites[1];
			System.out.println(url);
			driver.get(url);
			String expectedValue = websites[0];
			try {
				System.out.println("recieved title =" + driver.getTitle() + " expected title=" + expectedValue);
			Assert.assertEquals(driver.getTitle(),expectedValue);
			System.out.println("title matched");
			}
			catch (AssertionError e) {
				System.err.println("title mismatch" );
			}
			//System.out.println(
				//	"website name: " + websites[0] + " | website url: " + websites[1] + " | coded in: " + websites[2]);

		}

	}

}
