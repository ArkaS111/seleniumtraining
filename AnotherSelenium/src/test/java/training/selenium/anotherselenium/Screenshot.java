package training.selenium.anotherselenium;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.google.common.io.Files;

public class Screenshot {
	public String getScreenhot(WebDriver driver, String screenshotName) throws Exception {
		String dateName = "today";
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = "D:\\arka\\Documents\\LearnToCode\\Selenium\\" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		Files.copy(source, finalDestination);
		return destination;
	}

	public void openWebsite1(WebDriver driver) {
		
		driver.navigate().to("https://www.google.com");
		System.out.println("Opened website 1");
	}
public void openWebsite2(WebDriver driver) {
		
		driver.navigate().to("https://www.facebook.com");
		System.out.println("Opened website 2");
}
	public static void main(String[] args) throws Exception {
		Screenshot s = new Screenshot();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		System.setProperty("webdriver.chrome.driver",
				"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);
		s.openWebsite1(driver);
		s.getScreenhot(driver, "website1");
		s.openWebsite2(driver);
		s.getScreenhot(driver, "website2");
		driver.quit();
	}

}
