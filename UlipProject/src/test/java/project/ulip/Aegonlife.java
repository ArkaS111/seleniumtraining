package project.ulip;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class Aegonlife {
	String aegonDate;
	String aegonPrice;
	String aegonName = "Aegon Life" ;
	void selectSchemeName(WebDriver driver) {
	Select schemeName = new Select(driver.findElement(By.id("edit-field-scheme-tid")));
	schemeName.selectByValue("3");
	}
	void selectStartDate(WebDriver driver) {
		driver.findElement(By.id("edit-field-published-date-value-min-datepicker-popup-0")).click();
		driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight")).click();
	}
	void selectEndDate(WebDriver driver) {
		driver.findElement(By.id("edit-field-published-date-value-max-datepicker-popup-0")).click();
		driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight")).click();
	}
	void clickSearch(WebDriver driver) {
		driver.findElement(By.id("edit-submit-nav-rates")).click();
	}
	void pickDate(WebDriver driver) {
		aegonDate = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr/td[1]/span")).getText();
	}
	void pickPrice(WebDriver driver) {
		aegonPrice = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr/td[4]")).getText();
	}
	//*[@id="content"]/div[1]/div[2]/table/tbody/tr/td[4]
}
