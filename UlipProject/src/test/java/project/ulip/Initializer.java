package project.ulip;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Initializer {
	WebDriver init() {
	System.setProperty("webdriver.chrome.driver",
			"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
	return new ChromeDriver();
	}
	void openAegonlife(WebDriver driver) {
		driver.navigate().to("https://www.aegonlife.com/fund-performance/nav-rate");
	}
	void openTataaia(WebDriver driver) {
		driver.navigate().to("https://apps.tataaia.com/ULNAV/funds/daily-nav.jsp");
	}
	void openAdityabirla(WebDriver driver) {
		driver.navigate().to("https://lifeinsurance.adityabirlacapital.com/about-us/know-our-funds");
	}

	void closeWindow(WebDriver driver) {
		driver.close();
	}
}
