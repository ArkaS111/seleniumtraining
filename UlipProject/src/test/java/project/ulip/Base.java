package project.ulip;

import org.openqa.selenium.WebDriver;

public class Base {
	public static void main(String[] args) {
		Initializer initializer = new Initializer();
		WebDriver driver = initializer.init();
		initializer.openAegonlife(driver);
		Aegonlife al = new Aegonlife();
		al.selectSchemeName(driver);
		al.selectStartDate(driver);
		al.selectEndDate(driver);
		al.clickSearch(driver);
		al.pickDate(driver);
		al.pickPrice(driver);
		
		initializer.openAdityabirla(driver);
		Birla b = new Birla();
		b.pickDate(driver);
		b.pickPrice(driver);
		
		initializer.openTataaia(driver);
		Tata tt = new Tata();
		tt.stopLoad(driver);
		tt.selectFund(driver);
		tt.selectDay(driver);
		tt.selectMonth(driver);
		tt.selectYear(driver);
		tt.clickSubmit(driver);
		tt.pickDate(driver);
		tt.picPrice(driver);
		tt.stopLoad(driver);
		
		//System.out.println("Name,Date,Price");
		String[] aegon = {al.aegonName , al.aegonDate , al.aegonPrice};
		String[] birla = {b.birlaName , b.birlaDate,b.birlaPrice};
		String[] tata = { tt.tataName , tt.tataDate , tt.tataPrice};
		
		new CSVcreator().writeDataLineByLine(aegon, birla, tata);
		
		initializer.closeWindow(driver);
		
	}

}
