package grid.selenium;

import java.net.URL;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Grid {
	private static DesiredCapabilities capabillities;
	  private static WebDriver driver;

	  @BeforeClass
	  public static void setUp() throws Exception {  
	    capabillities = DesiredCapabilities.chrome();
	    /** URL is the selenium hub URL here **/
	    driver = new RemoteWebDriver(new URL("http://192.168.56.1:4444/wd/hub"), capabillities);
	    capabillities.setBrowserName("chrome");
	    capabillities.setPlatform(Platform.WIN8);
//	    new WebDriverWait(driver, 6000);
	  }

	  /**
	     * To test the UI
	     * @throws Exception
	     */
	  @Test
	  public void testUI() throws Exception {     

	    /** Your application URL which you want to test **/
	    driver.get("https://www.google.com"); 
	  }

	  @AfterClass
	  public static void tearDown() throws Exception {
	    driver.quit();  
	  }
}
