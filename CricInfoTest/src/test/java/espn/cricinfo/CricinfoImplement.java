package espn.cricinfo;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class CricinfoImplement {
	static ExtentTest test;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest logger;

	@BeforeTest
	public void startReport() {

		htmlReporter = new ExtentHtmlReporter("D:\\arka\\Download\\report1.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", "SoftwareTestingMaterialInceptial");
		extent.setSystemInfo("Environment", "Automation Testing");
		extent.setSystemInfo("User Name", "arka");

		htmlReporter.config().setDocumentTitle("Extent Tutorial");
		htmlReporter.config().setReportName("Report1");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.STANDARD);
	}

	@Test
	public void cricInfoRun() {
		Initializer in = new Initializer();
		in.initDriver();
		in.navigateEspncricinfo();
		in.maximizeWindow();
		in.waitToLoad();
		String title = "Live cricket scores, match schedules, latest cricket news, cricket videos";
		String fetchedValue = in.fetchPageTitle();
		CricinfoHomepage hm = new CricinfoHomepage();
		hm.clickOnElements();
		CricinfoLivescorePage clp = new CricinfoLivescorePage();
		clp.getMatchLinks();
		clp.clickMatchLinks();
		CricinfoMatchDetailsPage cmdp = new CricinfoMatchDetailsPage();
		// cmdp.getMatchDetails();
		// cmdp.toJson();
		in.closeDriver();
		logger = extent.createTest("Test1");
		Assert.assertEquals(title, fetchedValue);
		logger.log(Status.PASS, MarkupHelper.createLabel("Test Case Passed is passTest", ExtentColor.GREEN));
	}
//	@AfterMethod
//	public void getResult(ITestResult result) {
//		if (result.getStatus() == ITestResult.FAILURE) {
			// logger.log(Status.FAIL, "Test Case Failed is "+result.getName());
			// MarkupHelper is used to display the output in different colors
//			logger.log(Status.FAIL,
//					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
//			logger.log(Status.FAIL,
//					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
//		} else if (result.getStatus() == ITestResult.SKIP) {
//			// logger.log(Status.SKIP, "Test Case Skipped is "+result.getName());
//			logger.log(Status.SKIP,
//					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
//		}
//	}
	@AfterTest
	public void endReport() {
		extent.flush();
	}

}
