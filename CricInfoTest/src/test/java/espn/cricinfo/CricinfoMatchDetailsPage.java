package espn.cricinfo;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CricinfoMatchDetailsPage extends Initializer {
	protected ArrayList<WebElement> matchDetails = (ArrayList<WebElement>) driver
			.findElements(By.xpath("//div[@class='match-header-container']"));
	protected ArrayList<WebElement> matchScores = (ArrayList<WebElement>) driver
			.findElements(By.xpath("//div[@class='live-scorecard']/div[2]/div/div/div/div"));
	JSONArray finalArray = new JSONArray();

	public void getMatchDetails() {
		//for (int k = 0; k < 2; k++) {
			JSONObject obj = new JSONObject();
			JSONObject m = new JSONObject();
			for (int i = 1; i <= matchDetails.size(); i++) {
				obj.put("match details", (matchDetails.get(i-1).getText()));
				System.out.println(matchDetails.get(i-1).getText());
				for (int j = 1; j <= matchScores.size(); j++) {
					obj.put("match score", (matchScores.get(j-1).getText()));
					System.out.println(matchScores.get(j-1).getText());
					//m.put("matches", obj);
				}
			}
			m.put("matches", obj);
			finalArray.put(m);
		}


	public void toJson() {
		try {
			FileWriter file = new FileWriter("D:\\arka\\Download\\test1.json", true);
			file.write(finalArray.toString(1));
			file.flush();
			file.close();
			System.out.println(finalArray.toString(1));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
