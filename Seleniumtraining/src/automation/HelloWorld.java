package automation;

import java.util.Scanner;

public class HelloWorld {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of times you want to print hello :");
		int n = sc.nextInt();
		for(int i = 1; i <= n; i++) {
			System.out.println("Hello " + i);
		}
		

	}

}
