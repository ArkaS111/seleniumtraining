package automation;

import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WhatsappAutomate {

	public void whatsappAutomate() throws FileNotFoundException {
		try {
			System.setProperty("webdriver.chrome.driver",
					"D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			WebDriverWait wait = new WebDriverWait(driver, 30);
			driver.navigate().to("https://web.whatsapp.com/");
			driver.manage().window().maximize();
			WebElement nextPage;

			nextPage = wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//*[@id=\"side\"]/div[1]/div/label/div/div[2]")));
			nextPage.sendKeys("jio");
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(
					By.xpath("//*[@id=\"pane-side\"]/div[1]/div/div/div[1]/div/div/div[2]/div[1]/div[1]/span/span"))
					.click();

			for (int i = 0; i <= 10; i++) {
				driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]"))
						.sendKeys("Pecha koy pechani, khasa tor chechani");
				driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span"))
						.click();
			}
		} catch (NoSuchElementException e) {
			System.out.println("element not found");
		} catch (ElementNotInteractableException e) {
			System.out.println("element cannot be interacted");
		}

	}

}
