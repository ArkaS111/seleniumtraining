package automation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class SecondTest {

	public static void main(String[] args) {
		//System.setProperty("webdriver.edge.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\edgedriver_win64\\msedgedriver.exe");
		//WebDriver driver = new EdgeDriver();
		System.setProperty("webdriver.gecko.driver","D:\\arka\\Documents\\LearnToCode\\Selenium\\geckodriver-v0.29.1-win64\\geckodriver.exe" );   
		WebDriver driver= new FirefoxDriver(); 
		driver.navigate().to("https://www.testandquiz.com/selenium/testing.html");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sampleText = driver.findElement(By.className("col-md-12")).getText();
		System.out.println(sampleText);
		driver.findElement(By.id("fname")).sendKeys("Java");
		driver.findElement(By.id("fname")).clear();
		driver.findElement(By.id("idOfButton")).click();
		driver.findElement(By.id("male")).click();
		driver.findElement(By.cssSelector(".Automation")).click();
		Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
		dropdown.selectByVisibleText("Automation Testing");
		driver.findElement(By.linkText("This is a link")).click();
		driver.close();
		
	}

}
