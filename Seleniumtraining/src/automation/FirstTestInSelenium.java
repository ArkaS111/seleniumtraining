package automation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstTestInSelenium {

public static void main(String[] args) {
//	ChromeOptions options = new ChromeOptions();
//	options.addArguments("start-maximized");
//	options = options.setBinary("C:\\Users\\arka\\AppData\\Local\\Vivaldi\\Application\\vivaldi.exe");

//setting the driver executable
	System.setProperty("webdriver.edge.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\edgedriver_win64\\msedgedriver.exe");

//Initiating your chromedriver
	WebDriver driver=new EdgeDriver();
//	WebDriver driver=new ChromeDriver(options);

//Applied wait time
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//maximize window
//	driver.manage().window().maximize();

//open browser with desried URL
	driver.get("https://www.google.com");

//closing the browser
	driver.close();

	}

}