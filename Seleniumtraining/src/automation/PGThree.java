package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PGThree {
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver","D:\\arka\\Documents\\LearnToCode\\Selenium\\geckodriver-v0.29.1-win64\\geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
        String alertMessage = "";

        driver.get("http://jsbin.com/usidix/1");
        driver.findElement(By.cssSelector("input[value=\"Go!\"]")).click();
        alertMessage = driver.switchTo().alert().getText();
        driver.switchTo().alert().accept();
       
        System.out.println(alertMessage);
        driver.quit();
       
    }

}
