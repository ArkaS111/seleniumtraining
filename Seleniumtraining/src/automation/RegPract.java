package automation;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RegPract {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//System.setProperty("webdriver.edge.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\edgedriver_win64\\msedgedriver.exe");
		//WebDriver driver = new EdgeDriver();
		//System.setProperty("webdriver.gecko.driver","D:\\\\arka\\\\Documents\\\\LearnToCode\\\\Selenium\\\\geckodriver-v0.29.1-win64\\\\geckodriver.exe" );    
	    //WebDriver driver= new FirefoxDriver();
		driver.navigate().to("http://automationpractice.com/index.php");
		//driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();
		driver.findElement(By.xpath("//input[@id=\"email_create\"]")).sendKeys("pqrstu@email.com");
		driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]/span")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"id_gender1\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"customer_firstname\"]")).sendKeys("xyz");
		driver.findElement(By.xpath("//*[@id=\"customer_lastname\"]")).sendKeys("zyx");
		driver.findElement(By.xpath("//*[@id=\"passwd\"]")).sendKeys("abcd1");
		Select dropdown = new Select(driver.findElement(By.id("days")));
		dropdown.selectByValue("11");
	    Select dropdown1 = new Select(driver.findElement(By.id("months")));
	    dropdown1.selectByValue("4");
	    Select dropdown2 = new Select(driver.findElement(By.id("years")));
	    dropdown2.selectByValue("1994");
		driver.findElement(By.xpath("//*[@id=\"address1\"]")).sendKeys("vusondir math");
		driver.findElement(By.xpath("//*[@id=\"city\"]")).sendKeys("New York");
		Select dropdown3 = new Select(driver.findElement(By.id("id_state")));
	    dropdown3.selectByVisibleText("New York");
	    driver.findElement(By.xpath("//*[@id=\"postcode\"]")).sendKeys("00001");
	    driver.findElement(By.xpath("//*[@id=\"phone_mobile\"]")).sendKeys("700986543");
	    driver.findElement(By.xpath("//*[@id=\"submitAccount\"]/span")).click();

	}

}
